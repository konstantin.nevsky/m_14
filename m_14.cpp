// m_14.cpp
//

#include <iostream>
#include <string>

int main()
{
    std::string Var{"Hello Skillbox!"};
    std::cout << Var << "\n";
    std::cout << Var.length() << "\n";
    std::cout << Var.front() << "\n";
    std::cout << Var.back() << "\n";
}

